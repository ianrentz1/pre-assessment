package com.gitlab.ianrentz1;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class AdderApplicationTests {

	@Test
	public void contextLoads() {
	}

	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void testUsage() {
		String body = this.restTemplate.getForObject("/", String.class);
		assertThat(body).isEqualTo("Enter two numbers, separated by the path separator (for example: http://example.com/1/2). The application will output their sum.");
	}

	@Test
	public void testSum_validInput() {
		Long body = this.restTemplate.getForObject("/1/2", Long.class);
		assertThat(body).isEqualTo(3);
	}

	@Test
	public void testSum_invalidInput1() {
		expectedException.expectMessage(("num1 or num2 was null. Please enter a valid number for each."));
		expectedException.expect(RuntimeException.class);
		new AdderApplication().sum(null, 1L);
	}

	@Test
	public void testSum_invalidInput2() {
		expectedException.expectMessage(("num1 or num2 was null. Please enter a valid number for each."));
		expectedException.expect(RuntimeException.class);
		new AdderApplication().sum(1L, null);
	}
}
