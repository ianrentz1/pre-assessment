package com.gitlab.ianrentz1;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class AdderApplication {

	@GetMapping("/")
	String usage() {
		return "Enter two numbers, separated by the path separator (for example: http://example.com/1/2). The application will output their sum.";
	}

	@GetMapping("/{num1}/{num2}")
	@ResponseBody
	Long sum(@PathVariable Long num1, @PathVariable Long num2) {
		if (num1 == null || num2 == null) {
			throw new RuntimeException("num1 or num2 was null. Please enter a valid number for each.");
		}
		return num1 + num2;
	}

	public static void main(String[] args) {
		SpringApplication.run(AdderApplication.class, args);
	}
}